export async function render(): Promise<void> {
    //constants
    const API_KEY: string = 'bbc6d14b9d74d72eb1768f7e8ab9e814';
    const MOVIES_URL: string = `https://api.themoviedb.org/3/movie/popular?api_key=${API_KEY}&language=en-US&page=1`;
    const POPULAR_MOVIES_URL: string = `https://api.themoviedb.org/3/movie/popular?api_key=${API_KEY}&language=en-US&page=1`;
    const UPCOMING_MOVIES_URL: string = `https://api.themoviedb.org/3/movie/upcoming?api_key=${API_KEY}&language=en-US&page=1`;
    const TOP_RATED_MOVIES_URL: string = `https://api.themoviedb.org/3/movie/top_rated?api_key=${API_KEY}&language=en-US&page=1`;
    const SEARCH_URL: string = `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&language=en-US`;
    //const DETAILS = `https://api.themoviedb.org/3/movie/${movie_id}?api_key=${API_KEY}&language=en-US`;
    //function render card
    const createElement = (
        src: string,
        date: string,
        overview: string,
        id: string
    ) => {
        const card = document.createElement('div');
        card.classList.add('col-lg-3', 'col-md-4', 'col-12', 'p-2');
        card.innerHTML = `  <div class="card shadow-sm">
                                <img
                                    src="https://image.tmdb.org/t/p/original/${src}"
                                />
                                <svg
                                id=${id}
                                    xmlns="http://www.w3.org/2000/svg"
                                    stroke="red"
                                    fill="transparent"
                                    width="50"
                                    height="50"
                                    class="bi bi-heart-fill position-absolute p-2"
                                    viewBox="0 -2 18 22"
                                >
                                    <path
                                    id="favorite"
                                        fill-rule="evenodd"
                                        d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                                    />
                                </svg>
                                <div class="card-body">
                                    <p class="card-text truncate">
                                        ${overview}
                                    </p>
                                    <div
                                        class="d-flex justify-content-between align-items-center"
                                    >
                                        <small class="text-muted"
                                            >${date}</small
                                        >
                                    </div>
                                </div>
                            </div>`;
        return card;
    };

    //value Dom
    const container: any = document.querySelector('#film-container');
    const loaderHTML: any = document.querySelector('#loader');
    const loadMore: any = document.querySelector('#load-more');

    //Loader
    const toggleLoader = () => {
        const isHidden = loaderHTML.hasAttribute('hidden');
        if (isHidden) {
            loaderHTML.removeAttribute('hidden');
        } else {
            loaderHTML.setAttribute('hidden', '');
        }
    };

    //function render movies
    const getAllMovies = (url: string) => {
        toggleLoader();
        const result = fetch(url, {
            method: 'GET',
        });
        //console.log('result', result);
        result
            .then((response) => {
                if (!response.ok) {
                    throw new Error('Помилка запиту');
                }
                return response.json();
            })
            .then((movies) => {
                // console.log('movies', movies.results);
                console.log(movies.results);
                let id: string[] = [];
                const arr = movies.results;
                arr.forEach((movie: any) => {
                    if (movie.poster_path === null) {
                        throw new Error('Картинки не знайдено');
                    }
                    const movieHTML = createElement(
                        movie?.poster_path,
                        movie.release_date,
                        movie.overview,
                        movie.id
                    );
                    container.append(movieHTML);
                    // id.push(String(movie.id));
                });
                // localStorage.setItem('storeID', JSON.stringify(id));
            })
            .catch((error) => {
                console.log('error', error);
            })
            .finally(() => {
                toggleLoader();
            });
    };
    getAllMovies(MOVIES_URL);
    console.log(localStorage);
    //pagination
    let page = 2;
    loadMore?.addEventListener('click', (event: any) => {
        if (page <= 5) {
            const url = `https://api.themoviedb.org/3/movie/popular?api_key=${API_KEY}&language=en-US&page=${page}`;
            getAllMovies(url);
            page++;
        } else {
            loadMore.classList.add('btn', 'btn-lg', 'btn-outline-danger');
            loadMore.textContent = `It's All...`;
        }
    });

    // function for selected movies
    const popular: any = document.querySelector('#popular');
    const upcoming: any = document.querySelector('#upcoming');
    const topRated: any = document.querySelector('#top_rated');

    const selectedMovies = (item: any, url: string) => {
        item?.addEventListener('click', (event: any) => {
            container.innerHTML = ``;
            getAllMovies(url);
        });
    };
    //popular
    selectedMovies(popular, POPULAR_MOVIES_URL);
    //upcoming
    selectedMovies(upcoming, UPCOMING_MOVIES_URL);
    //top rated
    selectedMovies(topRated, TOP_RATED_MOVIES_URL);

    //searchMovies
    const form = document.querySelector('.form-inline');
    const search: any = document.querySelector('#search');
    form?.addEventListener('submit', (event) => {
        event.preventDefault();
        if (search.value) {
            console.log(search.value);
            container.innerHTML = '';
            getAllMovies(SEARCH_URL + '&query=' + search.value);
        } else {
            getAllMovies(MOVIES_URL);
        }
    });

    //function for add and delete favorite movies in localstorage
    const favoriteMovies = () => {
        let storageId: string[] = [];
        container?.addEventListener('click', (event: any) => {
            const { target } = event;
            const icon = target.closest('.bi-heart-fill');
            if (icon) {
                const findElement = storageId.findIndex((e) => e === icon.id);
                if (findElement === -1) {
                    storageId.push(String(icon.id));
                    localStorage.setItem('storeID', JSON.stringify(storageId));
                    icon.setAttribute('fill', 'red');
                } else {
                    console.log('delete');
                    storageId = storageId.filter((e) => e !== String(icon.id));
                    localStorage.setItem('storeID', JSON.stringify(storageId));
                    icon.setAttribute('fill', 'transparent');
                }
            } else {
                console.log('no');
            }
        });
    };
    favoriteMovies();
    //localStorage.clear();
}
